<?php
namespace Oliverbode\OwlWidgetSlider\Block\Widget;

class Slider extends \Oliverbode\OwlWidgetSlider\Block\Slider implements \Magento\Widget\Block\BlockInterface
{
    public function getTemplate()
    {
        if (is_null($this->_template)) {
            $this->_template = 'Oliverbode_OwlWidgetSlider::slider.phtml';
        }
        return $this->_template;
    }
}
