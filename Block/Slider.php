<?php
namespace Oliverbode\OwlWidgetSlider\Block;

use Magento\Cms\Model\Template\FilterProvider;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Cms\Model\BlockFactory;
use Magento\Framework\View\Element\Template\Context;

class Slider extends \Magento\Framework\View\Element\Template
{

    protected $filterProvider;

    protected $blockFactory;

    public function __construct(
        Context $context,
        FilterProvider $filterProvider,
        BlockFactory $blockFactory,
        array $data = []
    ) {
        parent::__construct($context,$data);
        $this->_filterProvider = $filterProvider;
        $this->_blockFactory = $blockFactory;
    }

    private function getBlockIds() {
        return explode(',',$this->getBlocks());
    }

    public function getSliderOptions()
    {   
        return $this->getOptions();
    }

    public function getSlides()
    {   
        $storeId = $this->_storeManager->getStore()->getId();
        $slides = array();
        $blockIds = $this->getBlockIds();
        foreach ($blockIds as $blockId)  {
            $block = $this->_blockFactory->create();
            $block->setStoreId($storeId)->load($blockId);           
            $slides[] = $this->_filterProvider->getBlockFilter()->setStoreId($storeId)->filter($block->getContent());
        }
        return $slides;
    }
}