# Magento2 Owl Widget Slider #

Magento 2 Widget Slider using the Owl effect

## Features ##
* Well organized, easy to use and professionally designed.
* Uses the Magento 2 core Cms widget/blocks/pages functionality. 
* Ability to add multiple slides
* Built on the smoothslides library with all options configurable
* Comes installed with a demo that can be viewed at {Magento2 Base Url}/ken-burns-slider/
* Can be installed without demo simply by removing the Setup directory.

## Install Magento 2 Owl Widget Slider ##
### Manual Installation ###

Install Owl Widget Slider

* Download the extension
* Unzip the file
* Create a folder {Magento root}/app/code/Oliverbode/OwlWidgetSlider
* Copy the content from the unzip folder
* Optionally remove Setup folder if you don't want to install the demo

### Composer Installation ###

```
#!

composer config repositories.oliverbode_owlwidgetslider vcs https://oliver_bode@bitbucket.org/oliver_bode/oliverbode_owlwidgetslider.git
composer require oliverbode/owlwidgetslider
```

## Enable Owl Widget Slider ##

```
#!

php -f bin/magento module:enable --clear-static-content Oliverbode_OwlWidgetSlider
php -f bin/magento setup:upgrade
php -f bin/magento setup:static-content:deploy
```

## Creating a Owl Widget Slider ##

Log into your Magetno Admin, then goto Content -> Blocks

Create two or more blocks and upload image into content area. These will be the slides.

### Create on a single page ###

Goto Content -> Pages and either create or open an existing page.

In the content area select "Hide Editor" and Insert Widget. Choose the "Owl Widget Slider" fill in the widget options select the blocks you previously created and insert the widget.

### Create on one or more pages ###

Goto Content -> Widget and create a new widget. Choose the "Owl Widget Slider"

Select the pages where you wish to insert the widget and the block where you would like it to display. In the "Widget Options" select the blocks you previously created, optionally add the widget options and save widget.


### Removing Demo Owl Widget Slider ###

The demo can be viewed at: {Magento2 Base Url}/owl-slider/

To remove it: log into your Magetno Admin, then goto Content -> Pages

Delete the "Owl Slider".

Goto Content -> Blocks and delete the three blocks: Owl Slide (1,2 & 3)